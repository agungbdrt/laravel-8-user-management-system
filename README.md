#LARAVEL 8 - Basic User Management System

this App is basic user management system CRUD. this app implementing a couple of basic feature in
laravel 8 for learner.
* Feature :
    * CRUD
    * Data Integrating
      * PostgreSql (Database)
      * Model (Laravel Eloquent ORM)
      * Migration
      * Seed / Laravel Tinker
      * Validation
    * View
      * Blade Templating
      * Pagination  
      * Bootstrap 5
      * SASS
      * post-processing using Mix Webpack
      * Exception View
    * AUTH & oauth
      * Controller
      * Login System (Laravel Fortify)
      * Route
        * Prefix
        * Grouping
        * Naming
        * Resource
      * Middleware
        * Custom Middleware
        * Gate
        * 'can' access feature
    * etc...
